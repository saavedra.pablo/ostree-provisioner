# -*- coding:utf-8 -*-
#
# Author: Pablo Saavedra
# Maintainer: Pablo Saavedra
# Contact: psaavedra at igalia.com

import json
import re
import requests
import time

from threading import Thread

from .utils import datestring2time
from .utils import debug_conffile
from .utils import debug_provisioning_data
from .utils import execute_command
from .utils import get_logger
from .utils import get_provisioning_data
from .utils import is_file
from .utils import rm_file
from .utils import time2datestring

logger = get_logger()


def ostree_set_remote(ostree_repo_url, ostree_branch,
                      ostree_gpg_verify, ostree_tls_client,
                      settings={'ostree_auth':{}}):
    logger.debug("ostree_set_remote")
    f =  "/etc/ostree/remotes.d/ostree_provisioner.conf"
    try:
        with open(f, "w") as remote_conf_file:
            remote_conf_file.write('[remote "ostree_provisioner"]\n')
            remote_conf_file.write("url=%s\n" % ostree_repo_url)
            remote_conf_file.write("branches=%s;\n" % ostree_branch)
            if not ostree_gpg_verify:
                remote_conf_file.write("gpg-verify=false\n")
            if ostree_tls_client:
                if settings['ostree_auth']['tls_permissive']:
                    remote_conf_file.write("tls-permissive=true\n")
                remote_conf_file.write("tls-client-cert-path=%s\n" % \
                    settings['ostree_auth'].get('tls_client_cert_path', ''))
                remote_conf_file.write("tls-client-key-path=%s\n" % \
                    settings['ostree_auth'].get('tls_client_key_path', ''))
                remote_conf_file.write("tls-ca-path=%s\n" % \
                    settings['ostree_auth'].get('tls_ca_path', ''))
    except Exception as e:
        logger.error("ostree_set_remote: %s" % e)


def ostree_remote_gpg_import(ostree_gpg_key):
    cmd_ = "/usr/bin/ostree remote gpg-import ostree_provisioner -k %s" % (ostree_gpg_key)
    logger.debug("ostree_remote_gpg_import: %s" % cmd_)
    execute_command(cmd_, logger=logger, wait=True)


def ostree_pull(ostree_branch):
    cmd_ = "/usr/bin/ostree pull ostree_provisioner %s" % (ostree_branch)
    logger.debug("ostree_pull: %s" % cmd_)
    execute_command(cmd_, logger=logger, wait=True)


def ostree_show(ostree_branch):
    cmd_ = "/usr/bin/ostree show ostree_provisioner:%s" % (ostree_branch)
    logger.debug("ostree_show: %s" % cmd_)
    p = execute_command(cmd_, stdout=True, logger=logger, wait=True)
    res = {'commit': ''}
    if not p:
        return res
    for l in p.stdout:
        try:
            if l.find(b'commit') > -1:
                res['commit'] = l.split()[-1]
                logger.debug("ostree_show: %s" % res)
                return res
        except Exception:
            logger.error("Error parsing '%s' output" % cmd_)
    return res


def ostree_admin_deployed():
    cmd_ = "/usr/bin/ostree admin status"
    logger.debug("ostree_admin_status: %s" % cmd_)
    p = execute_command(cmd_, stdout=True, stderr=True,
                        logger=logger, wait=True)
    res = []
    if not p:
        return res
    for l in p.stderr:
        try:
            logger.error("ostree_admin_status:" % l)
        except Exception as e:
            logger.error("Error parsing '%s' output: %s" % (cmd_, e))
    for l in p.stdout:
        try:
            if l.find(b'.') > -1 and l.find(b'origin refspec') < 0:
                res.append(l.split(b'.')[0].split()[-1])
        except Exception as e:
            logger.error("Error parsing '%s' output: %s" % (cmd_, e))
    logger.debug("ostree_admin_deployed: %s" % res)
    return res


def ostree_admin_status():
    cmd_ = "/usr/bin/ostree admin status"
    logger.debug("ostree_admin_status: %s" % cmd_)
    p = execute_command(cmd_, stdout=True, stderr=True,
                        logger=logger, wait=True)
    res = {
        'pending': False,
    }
    if not p:
        return res
    for l in p.stderr:
        try:
            logger.error("ostree_admin_status:" % l)
        except Exception as e:
            logger.error("Error parsing '%s' output: %s" % (cmd_, e))
    first_line = True
    for l in p.stdout:
        try:
            if first_line:
                first_line = False
                res['commit'] = l.split(b'.')[0].split()[-1]
            if l.find(b'pending') > -1:
                res['pending'] = True
        except Exception as e:
            logger.error("Error parsing '%s' output: %s" % (cmd_, e))
    logger.debug("ostree_admin_status: %s" % res)
    return res


def ostree_admin_deploy(ostree_branch):
    cmd_ = "/usr/bin/ostree admin deploy ostree_provisioner:%s" % ostree_branch
    logger.debug("ostree_admin_deploy: %s" % cmd_)
    return execute_command(cmd_, logger=logger, wait=True)


def ostree_reboot():
    cmd_ = "/sbin/reboot"
    logger.debug("ostree_reboot: %s" % cmd_)
    return execute_command(cmd_, logger, wait=True)


def ostree_touch(filepath):
    cmd_ = "/bin/touch %s" % filepath
    logger.debug("ostree_touch: %s" % cmd_)
    return execute_command(cmd_, logger, wait=True)


def ostree_execute(command):
    logger.debug("ostree_execute: %s" % command)
    return execute_command(command, logger, wait=True)


class Upgrader(Thread):
    def __init__(self, settings):
        super(Upgrader, self).__init__()
        self.daemon = True
        self.provisioned = False
        self.timer = -1
        self.provisioning_info = None
        self.settings = settings
        self.reboot = False

    def print_status(self):
        debug_status = "upgrader status: ["
        debug_status += " timer: %s," % time2datestring(self.timer)
        debug_status += " provisioned: %s," % self.provisioned
        debug_status += " reboot: %s" % self.reboot
        debug_status += "]"
        return debug_status

    def alive(self):
        ostree_touch(self.settings['global']['check_alive'])

    def do_reboot(self):
        self.alive()
        ostree_touch(self.settings['global']['reboot_mark'])
        logger.debug("Stablished reboot mark")
        # XXX. Set the enviroment like OK for the next
        # reboot.
        ostree_execute(
            self.settings['global']['check_command_ok']
        )
        ostree_reboot()

    def run(self):
        while True:
            logger.debug(self.print_status())
            # Set as alive the system
            self.alive()
            logger.info("Checking for upgrades ...")
            self.check_upgrades()
            time.sleep(float(self.settings['global']['ostreepullinterval']))

    def check_upgrades(self):
        if not self.provisioned:
            logger.warning("Device not yet provisioned")
            return

        # Upgrade is skipped if there is not its time yet or if the time
        # is negative (a negative value means never)
        now = time.time()
        if 0 > self.timer:
            logger.debug("Invalid timer (timer: %s < 0). Skipped" % self.timer)
            return
        if self.timer > now:
            logger.debug(
                "Invalid timer (timer: %s > %s). Skipped" % (self.timer, now)
            )
            return

        # Do update for upgrades
        p = self.provisioning_info
        p_ostree_repo_url = p.get('ostree_repo_url', '')
        p_ostree_branch = p.get('ostree_branch', '')
        p_ostree_tls_client = p.get('tls_client', False)
        p_ostree_gpg_verify = p.get('gpg_verify', False)

        ostree_set_remote(p_ostree_repo_url,
                          p_ostree_branch,
                          p_ostree_gpg_verify,
                          p_ostree_tls_client,
                          self.settings
        )
        ostree_pull(p_ostree_branch)

        time.sleep(5)  # XXX Artificial sleep to get the updated status
        # of the the remote repository

        deploy_status = ostree_admin_status()
        ostree_commit = ostree_show(p_ostree_branch)['commit']

        if ostree_commit == '':
            logger.error("Not current ostree local repository commit")
            return

        if ostree_commit in ostree_admin_deployed():
            logger.debug("Commit %s skipped. Already deployed" % ostree_commit)
            return  # nothing to do if the commit is already deployed

        if deploy_status['commit'] != ostree_commit:
            p = ostree_admin_deploy(p_ostree_branch)
            logger.info("New image deployed: %s" % ostree_commit)
            if p.returncode != 0:
                logger.error("Uncontrolled error during the deploy")
                # TODO: recovery action here is required

        time.sleep(5)  # XXX Artificial sleep to get the updated status
        # of the the deploy

        # Only reboot the systems if we want apply the changes
        # immediately
        if self.reboot and ostree_admin_status()['pending']:
            logger.info("Rebooting system")
            self.do_reboot()


class Provisioner():
    def __init__(self, settings):
        self.settings = settings
        self.provisioning_data = get_provisioning_data(
            settings['global']['provisioningfile'])
        debug_conffile(settings, logger)
        debug_provisioning_data(self.provisioning_data, logger)
        self.upgrader = Upgrader(settings)
        self.upgrader.start()
        ostree_execute(self.settings['global']['check_command_init'])
        self.check_command_always = \
            self.settings['global']['check_command_always']
        if self.check_command_always:
            logger.debug("The check_command will executed always")
        self.after_reboot = is_file(self.settings['global']['reboot_mark'])
        if self.after_reboot:
            logger.debug("Reboot mark found")
            rm_file(self.settings['global']['reboot_mark'])
        self.headers = {
            'User-Agent': 'ostree-provisioner',
        }


    def run(self):
        ok_check_command = True
        ok_get_provisioning_info = False
        if not self.check_command_always and not self.after_reboot:
            ok_get_provisioning_info = True

        ostree_gpg_key = self.settings['ostree_auth'].get('gpg_key', '')
        if ostree_gpg_key != '':
            ostree_remote_gpg_import(ostree_gpg_key)

        while True:
            logger.debug("Refreshing provisioning information")
            # check if check_command is executed only after the first
            # boot after a deploy.
            if self.check_command_always or self.after_reboot:
                p = ostree_execute(
                    self.settings['global']['check_command']
                )
                if p.returncode != 0:
                    ok_check_command = False

            next_iter_sleep = float(self.settings['global']['pullinterval'])
            try:
                p = self.get_provisioning_info()
                # TODO: Check the last-modification time header to know
                # when to do the push action really.
                logger.debug(p)
                self.upgrader.provisioning_info = p
                deploy_time = p['deploy_time']
                deploy_mode = p.get('deploy_mode', "next_reboot")
                logger.info(
                    "Provisioning deploy: [deploy_time: %s, deploy_mode: %s]"
                    % (deploy_time, deploy_mode)
                )
                if deploy_time == 'immediately':
                    self.upgrader.timer = time.time()
                elif deploy_time == 'interval':
                    interval_start = datestring2time(p['interval_start'])
                    interval_end = datestring2time(p['interval_end'])
                    start = time2datestring(interval_start)
                    end = time2datestring(interval_end)
                    _m = "Received deploy window"
                    _m += " [%(interval_start)s - %(interval_end)s]" % p
                    logger.info(_m)
                    logger.debug(
                      "Current deploy window between %s and %s" % (start, end)
                    )
                    if interval_end > time.time():
                        self.upgrader.timer = interval_start
                    else:
                        self.upgrader.timer = -1
                else:
                    self.upgrader.timer = -1
                if deploy_mode == 'immediately':
                    self.upgrader.reboot = True
                self.upgrader.provisioned = True
                ok_get_provisioning_info = True
            except Exception as e:
                logger.error(
                    "Error pulling the provisioning information: %s " % e)
                next_iter_sleep = float(
                    self.settings['global']['pullintervalretry']
                )

            if ok_get_provisioning_info and ok_check_command:
                ostree_execute(
                    self.settings['global']['check_command_ok']
                )
            logger.debug(
                "Provisioning pull retry in: %d seconds" % next_iter_sleep
            )
            time.sleep(next_iter_sleep)


    def get_auth(self):
        if self.settings['provisioner_auth'].get('type', 'none') == 'http_basic' \
                and "username" in self.settings['provisioner_auth'] \
                and "password" in self.settings['provisioner_auth']:
            return (self.settings["provisioner_auth"]["username"],
                    self.settings["provisioner_auth"]["password"])
        return None


    def get_tls_client(self):
        if self.settings['provisioner_auth'].get('type', 'none') == 'tls_client' \
                and "tls_client_cert_path" in self.settings['provisioner_auth'] \
                and "tls_client_key_path" in self.settings['provisioner_auth']:
            return (self.settings["provisioner_auth"]["tls_client_cert_path"],
                    self.settings["provisioner_auth"]["tls_client_key_path"])
        return None


    def get_ca(self):
        if self.settings['provisioner_auth'].get('tls_permissive', True):
            return False
        if "tls_ca_path" in self.settings['provisioner_auth']:
            return self.settings["provisioner_auth"]["tls_ca_path"]
        return None


    def do_get_action(self, str_):
        logger.debug(str_)
        auth = self.get_auth()
        ca = self.get_ca()
        cert = self.get_tls_client()
        return requests.get(str_, auth=auth, cert=cert, verify=ca,
                            headers=self.headers)


    def do_post_action(self, str_, data={}):
        logger.debug(str_)
        auth = self.get_auth()
        ca = self.get_ca()
        cert = self.get_tls_client()
        return requests.post(str_, data=data, auth=auth, cert=cert,
                             verify=ca,
                             headers=self.headers)

    def get_provisioning_info(self):
        url = self.settings["global"]["url"]
        self.provisioning_data.get('OSTREE_HOSTID', '')

        url = url + re.sub(
            '//+', '/',
            self.settings['global']['provisioningmap'] % self.provisioning_data
        )
        r = self.do_get_action(url)
        logger.info("Action %s - %s: %s" % ("get_provisioning_info",
                    url, r.status_code))
        res = json.loads(r.content)
        return res
