#!/usr/bin/env python
# -*- coding:utf-8 -*-
#
# Author: Pablo Saavedra
# Maintainer: Pablo Saavedra
# Contact: psaavedra at igalia.com

import imp
import logging
import os
import pytz
import re
import sys
import time
if (sys.version_info > (3, 0)):
    import configparser
else:
    import ConfigParser as configparser

from dateutil.parser import parse
from datetime import datetime
from subprocess import Popen, PIPE

DEV_NULL = open('/dev/null', "w")


def datestring2time(datestring):
    d = parse(datestring)
    return time.mktime(d.timetuple())


def time2datestring(time):
    return datetime.fromtimestamp(time, tz=pytz.utc).isoformat()


def is_file(filepath):
    return os.path.isfile(filepath)


def rm_file(filepath):
    return os.remove(filepath)


def execute_command(cmd, stdout=False, stderr=False, logger=None,
                    wait=False):
    stderr = PIPE if stderr else DEV_NULL
    stdout = PIPE if stdout else DEV_NULL
    try:
        p = Popen(cmd.split(), shell=False, bufsize=1024,
                  stdin=PIPE, stdout=stdout,
                  stderr=stderr, close_fds=True)
        if wait:
            p.wait()
        return p
    except Exception as e:
        if logger:
            logger.error("Failed command (%s): %s" % (cmd, e))
        return None


def get_default_settings():
    settings = {}
    settings['ostree_auth'] = {
        'tls_permissive': 1,
    }
    settings['provisioner_auth'] = {
        'tls_permissive': 1,
    }
    settings['global'] = {
        'loglevel': 10,
        'logfile': '/dev/stdout',
        'url': 'http://localhost/ostree-provisioner/',
        'provisioningfile': '/etc/ostree-provisioner-data.cfg',
        'ostreepullinterval': 60,
        'pullinterval': 60,
        'pullintervalretry': 60,
        'refreshinterval': 10,
        'reboot_mark': './ostree.reboot.mark',
        'check_alive': '/tmp/ostree.alive.ok',
        'check_command_always': 0,
        'check_command': '/bin/true',
        'check_command_ok': '/bin/true',
        'check_command_init': '/bin/true',
    }
    return settings


def set_setting(settings, s, k, v):
    if s == "global":
        if k == "provisioningmap":
            settings[s][k] = re.sub('\$', '%', v)
        elif k == "loglevel":
            settings[s][k] = int(v)
        elif k == "check_command_always":
            if v.lower() in ("1", "true"):
                settings[s][k] = True
            else:
                settings[s][k] = False
        else:
            settings[s][k] = v
    elif s in ['ostree_auth', 'provisioner_auth']:
        if k == "tls_permissive":
            if v.lower() in ("1", "true"):
                settings[s][k] = True
            else:
                settings[s][k] = False
        else:
            settings[s][k] = v
    else:
        if s not in settings:
            settings[s] = {}
        settings[s][k] = v


def _setup(conffile, settings):
    try:
        imp.reload(sys)
        # Forcing UTF-8 in the enviroment:
        sys.setdefaultencoding('utf-8')
        # http://stackoverflow.com/questions/3828723/why-we-need-sys-setdefaultencodingutf-8-in-a-py-scrip
    except Exception:
        pass
    cfg = configparser.ConfigParser()
    cfg.read(conffile)
    for s in cfg.sections():
        for k, v in cfg.items(s):
            set_setting(settings, s, k, v)


def setup(conffile):
    settings = get_default_settings()
    _setup(conffile, settings)
    return settings


def debug_provisioning_data(provisioning_dict, logger):
    for k, v in provisioning_dict.items():
        logger.debug("Provisioning data - %s: %s" % (k, v))


def get_provisioning_data(provisioningfile):
    res = {}
    try:
        with open(provisioningfile) as f:
            loaded_data = dict(re.findall(r'(\S+)=(".*?"|\S+)', f.read()))
            for l in res.keys():
                res[l] = re.sub('"', '', loaded_data.get(l, res[l]))
    except IOError:
        pass
    return res


def debug_conffile(settings, logger):
    for s in settings.keys():
        for k in settings[s].keys():
            key = "%s.%s" % (s, k)
            value = settings[s][k]
            if k.find("passw") == -1:
                logger.debug("Configuration setting - %s: %s" % (key, value))


def create_logger(settings):
    hdlr = logging.FileHandler(settings["global"]["logfile"])
    hdlr.setFormatter(
        logging.Formatter('%(levelname)s %(asctime)s %(message)s'))
    logger = logging.getLogger('ostree-provisioner')
    logger.addHandler(hdlr)
    logger.setLevel(settings["global"]["loglevel"])
    logger.debug("Default encoding: %s" % sys.getdefaultencoding())
    return logger


def get_logger():
    return logging.getLogger('ostree-provisioner')
