## ostree-provisioner

### Quick start

* Install: pip install https://gitlab.com/saavedra.pablo/ostree-provisioner.git
* Configure:Create a ostree-provisioner.cfg and ostree-provisioner-data.cfg
* Run: ostree-provisioner -c ostree-provisioner.cfg

### Installation

`ostree-provisioner` is a Pythonic tool. The installation process is based
on setuptools so to get this tool installed in your system the simplest way
is just running the install trough `pip`:

    pip install https://gitlab.com/saavedra.pablo/ostree-provisioner.git

Alternative method:

    git clone https://gitlab.com/saavedra.pablo/ostree-provisioner.git
    python setup.py install

### Configuration file

The configuration file contains the basic setup of the `ostree_provisioner`
daemon. It is defined in INI format and. The general settings are defined in
the **global** section.

* `logfile`: Log file (default `/dev/stdout`).
* `loglevel`: Log messages verbosity (default 20).
* `url`: URL base (default http://localhost).
* `pullinterval`: Time between two provisioning requests (default 60).
* `pullintervalretry`: Time between two provisioning requests in case of fail
  (default 60).
* `provisioningfile`: Path of the configuration file in key=value format where
  are defined the values used for the `ostree-provisioner` to compose the
  provisioning URL.
* `provisioningmap`: Defines the how the provisioning URL is composed based.
  on the values of the keys contained in the `provisioningfile`.
* `ostreepullinterval`: Time between two ostree pull requests (default 60).


`check` properties set particular logic of the ostree-provisioner useful for
integration with dead-man systems like `watchman` or similars:

* `check_alive`: ostree-provider touch this file frequenly (<60s). Meanwhile it
  is up and running. Quite useful for watchdog supervision.
* `check_command`: Executable binary. Hook to add OK/KO logic after a
  provisioning query
* `check_command_init`: Executable binary. Hook to add logic to initialize
  the enviroment according with the check_command logic.
* `check_command_ok`: Executable binary. Hook to add logic after an OK
  in a provisioning query


`provisioner_auth`  and `ostree_auth` are the section which define the
authentication methods for the provisioning server and the `ostree_auth`

* `type`: Defines the authentication method. `none`, `http_basic` and
  `tls_client`. Only in `provisioner_auth`.
* `username`: Username. Only in `provisioner_auth`.
* `password`: Password. Only in `provisioner_auth`.
* tls_permissive: A boolean value. Server TLS certificates will
  be checked against the system certificate store. If this variable is
  set, any certificate will be accepted.
  Default: True
* tls_client_cert_path: Path to file for client-side certificate,
  to present when making requests to this repository.
  Default: /etc/ssl/certs/client.crt
* tls_client_key_path: Path to file containing client-side
  certificate key, to present when making requests to the
  repository.
  Default: /etc/ssl/private/client.key
* tls_ca_path: Path to file containing trusted anchors.
  Default: /etc/ssl/certs/ca.crt
* gpg_key: URL with the GPG public key of the OSTree signed repository .
  Default: -

The provisioning file is a key/value file without sections. This file
contains the keys which are used by the deamon to perform the provisioning
requests (see `cfg/ostree-provisioner-data.cfg.example`). The
`provisioningmap` setting defines how use these keys in the provisioning
URL. For example:

    /$(KEY_1)s/$(KEY_2)s/?param=$(OTHER_KEY_AS_PARAMETER)s


### Instructions

Daemon is running in foreground. This is the comand line for the execution
of the daemon:

    ostree-provisioner -c ostree-provisioner.cfg


### Provisioning URL content

    {
        "ostree_repo_url" : "http://10.0.0.1:57556",
        "ostree_branch" : "genericx86-64",
        "deploy_time" : "immediately",
        "deploy_mode" : "immediately",
        "id" : "OSTREE/DATACENTER-1/RACK-B/UNIT-4"
    }


* `deploy_time`:
  * Definition: Determines when the ostree pull will be executed in order
    to get (download) the available commits from the repository. There are
    two modes: immediately and interval. Meanwhile *immediately* allows the
    downloading of the new image once it is available, the *interval*
    option estawblishe a valid time window where this can be done. The
    interval can be defined as a fixed date or as a window during the day.
    The date also can be defined in localtime (naive) or related to a
    particular datetime. This is a mandatory value so must be explicitily
    defined in the JSON.
  * Mandatory: yes
  * Default: -
  * Accepted values: immediately, interval

* `interval_start`:
  * Definition: When **deploy_time** is set as *interval*, this
  values defines the start time of the scheduled window for the image
  check, pull and deploy.
  * Mandatory: No
  * Default: -
  * Accepted values: Date string

* `interval_end`:
  * Definition: When **deploy_time** is set as *interval*, this
  values defines the end time of the scheduled window for the image
  check, pull and deploy.
  * Mandatory: No
  * Default: -
  * Accepted values: Date string

* `deploy_mode`:
  * Definition: Determines if when should be applied the deploy of the new
    image. There are 2 valid values: *immediately* and *next_reboot*. When
    this setting is established to *immediately*, the image is applied once
    the deploy the pull and deploy is finished. The host will be
    immediately rebooted and it will reboot with the new image. In the
    other hand, *next_reboot* skips the reboot of the host. The image will
    be applied in the next reboot.
  * Mandatory: No
  * Default: next_reboot
  * Accepted values: immediately, next_reboot

* `gpg_verify`:
  * Definition: Controls whether OSTree will require commits to be
    signed by a known GPG key.
  * Mandatory: No
  * Default: False

* `tls_client`:
  * Definition: Client must use TLS client certificates to access to
    the repository
  * Mandatory: No
  * Default: False


### Date string format

Date string parameters supports a flexible syntax for the date definition.
The parsing of those strings is delegated to datetutil Python package.
All date formats valid for the dateutil parser are valid::

  from dateutil.parser import parse
  d = parse(datestring)

The parsers fullfits with the International Standard ISO 8601 numeric
representations of date and time (http://www.cl.cam.ac.uk/~mgk25/iso-time.html)
For simplicity, we could suggest this kind of date formats::

  00:00 (based in the localtime of the system)
  00:00 CEST
  00:00 UTC
  26 00:00:00
  26 00:00:00 UTC
  01-26 00:00:00
  01-26 00:00:00 UTC
  2018-01-26 00:00:00
  2018-01-26 00:00:00 UTC
  2018/01/26 00:00:00 CET
