from setuptools import setup, find_packages
import sys

if sys.version_info >= (3, 0):
    exec("def do_exec(co, loc): exec(co, loc)\n")
else:
    exec("def do_exec(co, loc): exec co in loc\n")


def get_version():
    d = {}
    try:
        do_exec(open("ostree_provisioner/__init__.py").read(), d)  # @UndefinedVariable
    except (ImportError, RuntimeError):
        pass
    return d["VERSION"]


version = get_version()

long_description = ""
try:
    long_description = file('README.md').read()
except Exception:
    pass

license = ""
try:
    license = file('LICENSE').read()
except Exception:
    pass


setup(
    name='ostree-provisioner',
    version=version,
    description='OSTREE daemon provisioner',
    author='Pablo Saavedra',
    author_email='psaavedra@igalia.com',
    url='https://gitlab.com/saavedra.pablo/ostree-provisioner.git',
    packages=find_packages(),
    package_data={
    },
    scripts=[
        'tools/ostree-provisioner',
    ],
    zip_safe=False,
    install_requires=[
        "python-dateutil",
        "pytz",
        "requests",
    ],
    data_files=[
        ('/usr/share/doc/ostree-provisioner/',
            ['cfg/ostree-provisioner.cfg.example',
             'cfg/ostree-provisioner-data.cfg.example']),
    ],

    download_url='https://gitlab.com/saavedra.pablo/meta-perf-browser/-/archive/master/ostree-provisioner.zip',
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Topic :: System :: Software Distribution",
        "Topic :: System :: Operating System",
    ],
    long_description=long_description,
    license=license,
    keywords="python ostree provisioner",
)
